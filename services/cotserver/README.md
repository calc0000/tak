# `cotserver`

The `cotserver` service provides a TCP CoT streaming endpoint, in the same vein as TAKserver. Effectively, it serves as a thin TCP interface to the `EventBus` instance at the core of the `tak` instance, with a few additional features:

* When a new client connects, some subset of the previously broadcast CoT events will be sent to the client by the server (see the `"semantics"` configuration key).
* When a client disconnects, a CoT event corresponding to the client becoming stale (after a certain period of time) is broadcast.

## Configuration

The `cotserver` service supports the following configuration options:

* `"uid"`: optional string, specifies a UID for the server to use. If left unspecified, a random string will be generated.
* `"jitter"`: optional bool (default false), specifies whether to enable a location jitter function. If enabled, a (relatively) small random value will be added to or subtracted from location values in broadcast events. This can be useful on a public server to protect the exact location of users.
* `"semantics"`: optional string (valid values: "legacy", "new") (default "new"), specifies the semantics set the server should use.
    * `"legacy"`
        * On client connect, CoT events corresponding to other connected clients are sent by the server.
    * `"new"`
        * On client connect, all previously broadcast CoT events are sent by the server.
* `"listen"`: required string, specifies the address to listen on, in the form `<host>:port`. The `<host>` part can be omitted to listen on all interfaces (e.g. `":8087"`).
