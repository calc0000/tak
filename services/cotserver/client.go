package cotserver

import (
	"io"
	"time"

	"github.com/davecgh/go-spew/spew"
	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
	"bitbucket.org/calc0000/tak/cot/model"
)

// Client is a registered TAK client.
type Client struct {
	// UID is the uid of this client.
	UID string

	// Channel is the transport to send/receive Events on.
	Channel *cot.Channel

	// server is the Server that is handling this client.
	server *Server

	// recv is a channel on which to receive events from a cot.EventBus.
	recv <-chan model.Event

	// handle is a handle to this client's subscription on a cot.EventBus.
	handle cot.Handle
}

// newClient creates a new Client.
func newClient(server *Server, uid string, ch *cot.Channel) *Client {
	return &Client{
		UID:     uid,
		Channel: ch,
		server:  server,
		handle:  server.bus.Subscribe(uid),
	}
}

// Close closes this client.
func (c *Client) Close() {
	c.handle.Close()
	c.Channel.Close()
}

// pong sends a ping message on this Client's channel. ATAK implementations will disconnect
// peer connections after ~20 seconds of no traffic.
func (c *Client) pong() error {
	now := time.Now()
	later := now.Add(20 * time.Second)

	// Just send a basic Event, with no details and no location.
	return c.send(model.Event{
		What: model.What{
			UID:  c.server.uid,
			Type: model.ParseTypeHint("t-x-c-t"),
		},
		When: model.When{
			Time:  model.Timestamp(now),
			Start: model.Timestamp(now),
			Stale: model.Timestamp(later),
		},
	})
}

// send an Event on this Client's Channel.
func (c *Client) send(ev model.Event) error {
	return c.Channel.Send(ev)
}

// run is the main loop for Clients.
func (c *Client) run(deregister chan<- string, rawLogger *zap.Logger) {
	// When we exit, tell the server that we're gone.
	defer func() {
		deregister <- c.UID
	}()

	logger := rawLogger.Sugar().Named("client:" + c.UID)

L:
	for {
		select {
		case ev := <-c.handle.Events:
			if err := c.send(ev); err != nil {
				logger.Errorf("failed to send event: %v", err)

				break L
			}
		case ev := <-c.Channel.Receive:
			if ev.Err != nil {
				// EOF is a clean disconnect.
				if ev.Err != io.EOF {
					logger.Errorf("received error: %v", ev.Err)
				}

				break L
			}

			// Special case for ping.
			if ev.UID == c.UID+"-ping" {
				if err := c.pong(); err != nil {
					logger.Errorf("failed to pong: %v", err)
					break L
				}

				// Handle this without bothering the server.
				continue
			}

			logger.Debugf(spew.Sdump(ev.Event))

			// Otherwise, just send this event to the bus.
			callsign, err := ev.Event.Detail.DestinationCallsign()
			if err != nil {
				logger.Errorf("failed to get event destination callsign: %v", err)
				continue
			}

			if callsign == "" {
				// This is a broadcast.
				c.server.bus.Broadcast(ev.Event)
			} else {
				// This is targeted.
				c.server.bus.PublishToCallsign(callsign, ev.Event)
			}
		}
	}
}
