// package cotserver contains a basic TAK CoT server.
//
// A CoT server provides rather basic functionality, since most of the business logic
// is contained on client devices. At its core, the server implemented here just relays
// CoT messages between clients as necessary, via a provided EventBus.
package cotserver

import (
	"fmt"
	"net"
	"time"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
	"bitbucket.org/calc0000/tak/cot/model"
	"bitbucket.org/calc0000/tak/services"
)

// newClientMessage is a message representing a new Client connection.
type newClientMessage struct {
	// Client is the client that just connected.
	Client *Client

	// Hello is the event it sent after connecting.
	Hello model.Event
}

// AttributedEvent is a model.Event with an optionally specified originator UID.
// If the originator UID is specified, this event will not be broadcast to it.
type AttributedEvent struct {
	// Originator is the UID of the client that originally sent this event.
	Originator string

	// Event is the event that was sent.
	model.Event
}

// Server is a TAK server, relaying COT messages between clients.
type Server struct {
	// uid is the uid suffix for this server.
	uid string
	// logger is the logger for this server.
	logger *zap.Logger
	// jitter is a location jitter function.
	jitter JitterFunc
	// semantics is the semantics set to use.
	semantics Semantics

	// bus is a CoT event bus.
	bus *cot.EventBus

	// clients is a map of client UID to *Client.
	clients map[string]*Client

	// newClients is a channel for client registration.
	newClients chan newClientMessage
}

// broadcast broadcasts the event to all connected clients.
func (s *Server) broadcast(ev AttributedEvent) error {
	for _, cl := range s.clients {
		if ev.Originator == cl.UID {
			continue
		}

		if err := cl.send(s.jitterEvent(ev.Event)); err != nil {
			// TODO: Have better error management here (kick the client?).
		}
	}

	return nil
}

// send sends an event to a Client, specified by UID, if it exists.
// It is not considered an error to specify an invalid UID (the client may have dropped).
func (s *Server) send(target string, ev AttributedEvent) error {
	cl, ok := s.clients[target]
	if !ok {
		return nil
	}

	return cl.send(s.jitterEvent(ev.Event))
}

// jitterEvent twiddles the location information in an event based on the Server's jitter function.
func (s *Server) jitterEvent(ev model.Event) model.Event {
	// Make a copy of the event.
	newEv := ev
	newEv.Point.Latitude = s.jitter(newEv.Point.Latitude)
	newEv.Point.Longitude = s.jitter(newEv.Point.Longitude)

	return newEv
}

// main is the main loop of the Server.
func (s *Server) main() {
	deregister := make(chan string)

	logger := s.logger.Named("main").Sugar()
	logger.Infof("starting")

	broadcasts := s.bus.SubscribeBroadcastOnly()
	defer broadcasts.Close()

	state := make(map[string]model.Event)

	for {
		select {
		case cl := <-s.newClients:
			// Record the callsign of this client.
			callsign, err := cl.Hello.Detail.Callsign()
			if err != nil {
				logger.Errorw("failed to get client callsign",
					"client", cl.Client.UID)
				continue
			}

			if callsign == "" {
				logger.Errorw("client hello had no callsign",
					"client", cl.Client.UID)
				continue
			}

			s.clients[cl.Client.UID] = cl.Client
			go cl.Client.run(deregister, s.logger)

			// Send this new client whatever objects we have in our state.
			now := time.Now()
			for uid, event := range state {
				if time.Time(event.When.Stale).Before(now) {
					delete(state, uid)
					continue
				}

				s.bus.PublishToUID(cl.Client.UID, event)
			}

		case uid := <-deregister:
			logger.Infow("deregistering client",
				"client", uid)
			s.clients[uid].Close()
			delete(s.clients, uid)

			// TODO: Is this the correct way to stale out a disconnected client?
			now := model.Timestamp(time.Now())
			earlier := model.Timestamp(time.Now().Add(-5 * time.Second))
			s.bus.Broadcast(model.Event{
				What: model.What{
					UID:  uid,
					Type: model.ParseTypeHint("a-f-G-U-C"),
				},
				When: model.When{
					Time:  now,
					Start: earlier,
					Stale: earlier,
				},
			})

		case ev := <-broadcasts.Events:
			if s.semantics == SemanticsLegacy {
				// Legacy semantics: don't record non-group contacts.
				group, err := ev.Detail.Group()
				if err != nil {
					logger.Errorw("failed to unmarshal Group from detail",
						"uid", ev.What.UID)
				} else if group == nil {
					continue
				}
			}

			state[ev.What.UID] = ev
		}
	}
}

// Start starts this Server, with the provided EventBus and config.
func (s *Server) Start(logger *zap.Logger, bus *cot.EventBus, decl services.Declaration) error {
	var config Config
	if err := decl.Decode(&config); err != nil {
		return err
	}

	s.uid = "server-" + config.uidOr()
	s.logger = logger.Named(s.uid)
	s.jitter = config.jitterOr()
	s.semantics = config.Semantics
	s.bus = bus
	s.clients = make(map[string]*Client)
	s.newClients = make(chan newClientMessage)

	go s.main()

	listener, err := net.Listen("tcp", config.ListenAddr)
	if err != nil {
		return err
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			return err
		}

		go func() {
			ch, err := cot.NewChannel(conn, conn)
			if err != nil {
				fmt.Printf("failed to create channel: %v", err)
				return
			}

			s.Register(ch)
		}()
	}

	return nil
}

// register reads the first event from a client Channel, and then registers the client.
func (s *Server) register(ch *cot.Channel) {
	// We need to receive the first event from this channel to get a uid.
	ev := <-ch.Receive
	if ev.Err != nil {
		s.logger.Named("register").Sugar().Infof("error receiving hello: %v", ev.Err)
		return
	}

	// Send this first event to the bus. Also pack it in the new client message so a
	// callsign can be extracted.
	s.bus.Broadcast(ev.Event)

	client := newClient(s, ev.UID, ch)

	s.logger.Sugar().Debugw(fmt.Sprintf("received hello %+v", ev.Event),
		"client", client.UID)

	s.newClients <- newClientMessage{client, ev.Event}
}

// Register registers a client Channel with this Server.
func (s *Server) Register(ch *cot.Channel) {
	go s.register(ch)
}

func init() {
	// Register ourselves.
	services.Register("cotserver", func() services.Service {
		return &Server{}
	})
}
