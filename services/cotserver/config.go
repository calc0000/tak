package cotserver

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
)

// Semantics is an enum of semantics modes.
type Semantics int

const (
	// SemanticsLegacy represents the default TAKserver semantics.
	// Specifically, on client connect: only other clients are sent.
	SemanticsLegacy Semantics = iota

	// SemanticsNew represents more sane behavior.
	// Specifically, on client connect: all objects are sent.
	SemanticsNew
)

// UnmarshalJSON configures this Semantics value from a string representation.
func (s *Semantics) UnmarshalJSON(b []byte) error {
	var repr string

	if err := json.Unmarshal(b, &repr); err != nil {
		return err
	}

	switch repr {
	case "legacy":
		*s = SemanticsLegacy
	case "new":
		*s = SemanticsNew
	default:
		return fmt.Errorf("unsupported semantics type: %v", repr)
	}

	return nil
}

// JitterFunc is an optionally specified location jitter function.
type JitterFunc func(float64) float64

// Jitter adjusts a provided float value, according to this jitter function.
func (f *JitterFunc) Jitter(v float64) float64 {
	if f == nil {
		return v
	}

	return (*f)(v)
}

// UnmarshalJSON configures this jitter function based on a JSON representation.
func (f *JitterFunc) UnmarshalJSON(b []byte) error {
	var enabled bool

	err := json.Unmarshal(b, &enabled)
	if enabled {
		*f = JitterFunc(func(v float64) float64 {
			magnitude := 0.02 + (rand.Float64() * 0.05)
			if rand.Int()%2 == 0 {
				magnitude *= -1
			}
			return v + magnitude
		})
	} else {
		*f = JitterFunc(func(v float64) float64 {
			return v
		})
	}

	return err
}

// Config is a Server config.
type Config struct {
	// UID is an optionally provided UID string for the server.
	UID string `json:"uid,omitempty"`

	// Jitter is an optionally provided location jitter function for the server.
	Jitter JitterFunc `json:"jitter"`

	// Semantics is the semantics to use for the server.
	Semantics Semantics `json:"semantics"`

	// ListenAddr is the string representation of the address to listen on.
	ListenAddr string `json:"listen"`
}

// uidOr returns the configured UID, or a random string.
func (c *Config) uidOr() string {
	if c != nil && c.UID != "" {
		return c.UID
	}

	uidBuf := make([]byte, 16)
	rand.Read(uidBuf)
	return base64.StdEncoding.EncodeToString(uidBuf)
}

// jitterOr returns the configured location jitter function, or identity.
func (c *Config) jitterOr() JitterFunc {
	return c.Jitter
}
