// package services provides types for declaring and working with TAK services.
package services

import (
	"encoding/json"
	"fmt"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
)

// Declaration is a service declaration, as usually specified in a config file.
type Declaration struct {
	// Service is the type name of the service referenced in this declaration.
	Service string

	// Config is the service-dependent config items.
	Config map[string]interface{}
}

// UnmarshalJSON unmarshals this Declaration from a JSON representation.
func (d *Declaration) UnmarshalJSON(b []byte) error {
	any := make(map[string]interface{})

	if err := json.Unmarshal(b, &any); err != nil {
		return err
	}

	serviceI, ok := any["service"]
	if !ok {
		return fmt.Errorf("declaration does not specify a service type")
	}

	service, ok := serviceI.(string)
	if !ok {
		return fmt.Errorf("service type value is not a string")
	}

	// Delete the "service" key.
	delete(any, "service")

	*d = Declaration{
		Service: service,
		Config:  any,
	}
	return nil
}

// Decode decodes the provided value from the arbitrary config in this Declaration.
func (d *Declaration) Decode(v interface{}) error {
	// First, marshal our config.
	b, err := json.Marshal(d.Config)
	if err != nil {
		return err
	}

	// Then, unmarshal into the provided value.
	return json.Unmarshal(b, v)
}

type Service interface {
	// Start instructs this service to start, using the provided EventBus, and unmarshaling
	// its config from the provided object.
	// Start may block.
	Start(logger *zap.Logger, bus *cot.EventBus, config Declaration) error
}

// registeredServices is a map of service kind to constructor function.
var registeredServices = make(map[string]func() Service)

// Register registers a Service constructor with a specified name.
func Register(kind string, f func() Service) {
	registeredServices[kind] = f
}

// Start starts a Service with the provided information.
func Start(logger *zap.Logger, bus *cot.EventBus, config Declaration) error {
	f, ok := registeredServices[config.Service]
	if !ok {
		return fmt.Errorf("unregistered service %s", config.Service)
	}

	service := f()
	return service.Start(logger, bus, config)
}
