// package federation implements basic TAK federation.
package federation

import (
	"crypto/tls"
	"fmt"
	"net"
	"sync/atomic"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
	"bitbucket.org/calc0000/tak/cot/model"
	"bitbucket.org/calc0000/tak/services"
)

// register is a message representing a new connection.
type register struct {
	// id is the id to use for this connection.
	id int64

	// channel is the channel to send and receive events on.
	channel *cot.Channel
}

// Federation is a federation service.
// The federation service can run in two modes:
//   * If "listen" is specified: listen for incoming federation connections
//   * If "connect" is specified: connect to a specified federation listener
// The federation service relays messages on an event bus to another server. This
// can be useful in cases such as two semi-trusted peers; each peer need only
// provide connectivity to the federated servers.
//
// From an event bus perspective, a federation service subscribes to events
// targeted at clients connected to its peers, and relays broadcast events
// globally. Filtering of events relayed between federated servers, if any,
// is always done on the sending side. That is, when a federation service
// receives an event, it should always handle it as appropriate (by either
// broadcasting it or publishing it for a specific client). Similarly, federation
// senders, whether in listen or connect mode, are responsible for relaying
// broadcasted messages to all peers.
type Federation struct {
	// uid is the UID of this service.
	uid string

	// logger is a logger to use for this service.
	logger *zap.Logger

	// config is the configuration of this service.
	config Config

	// bus is the EventBus to use for this service.
	bus *cot.EventBus

	// handle is the handle for all events on the EventBus.
	handle cot.AllHandle

	// recv is a channel on which Events received from peers are sent.
	recv chan model.Event

	// id is the next ID to use for connections. This value should
	// only be used via the nextID() method.
	id int64

	// register is a channel on which to send registration messages.
	register chan register

	// deregister is a channel on which to send deregistration messages.
	deregister chan int64
}

// nextID returns the next ID value to use for new connections.
func (f *Federation) nextID() int64 {
	return atomic.AddInt64(&f.id, 1)
}

// listen runs this federation service in listen mode.
func (f *Federation) listen() error {
	// Do our listening in a goroutine.
	go func() {
		logger := f.logger.Named("listen").Sugar()

		if err := func() error {
			var listener net.Listener
			var listenerErr error

			if f.config.CertificateChain != "" {
				// Listen using a TLS certificate (and chain).
				cert, err := tls.LoadX509KeyPair(f.config.CertificateChain, f.config.PrivateKey)
				if err != nil {
					return err
				}

				config := &tls.Config{
					Certificates: []tls.Certificate{cert},
					// TODO: Replace this with tls.RequireAndVerifyClientCert if client certificate verification is enabled.
					ClientAuth: tls.RequireAnyClientCert,
				}
				listener, listenerErr = tls.Listen("tcp", f.config.ListenAddr, config)
			} else {
				// Listen for raw TCP.
				listener, listenerErr = net.Listen("tcp", f.config.ListenAddr)
			}
			if listenerErr != nil {
				return listenerErr
			}

			logger.Infow("listening",
				"addr", f.config.ListenAddr)

			for {
				conn, err := listener.Accept()
				if err != nil {
					return err
				}

				// Now that we have a connection, create a channel.
				ch, err := cot.NewChannel(conn, conn)
				if err != nil {
					// We might be able to accept new connections, so don't bail in this case.
					logger.Errorf("failed to create channel for new client from %v: %v", conn.RemoteAddr(), err)
					continue
				}

				logger.Infow("accepted federation client",
					"peer", conn.RemoteAddr())

				// And register the channel.
				f.register <- register{f.nextID(), ch}
			}
			return nil
		}(); err != nil {
			f.logger.Named("listen").Sugar().Errorf("failed to listen for federation clients: %v", err)
		}
	}()

	f.main()
	return nil
}

// connect runs this federation service in connect mode.
func (f *Federation) connect() error {
	// Create our client in a goroutine.
	go func() {
		logger := f.logger.Named("connect").Sugar()

		if err := func() error {
			var conn net.Conn
			var connErr error

			if f.config.CertificateChain != "" {
				// Connect with a TLS client certificate (and chain).
				cert, err := tls.LoadX509KeyPair(f.config.CertificateChain, f.config.PrivateKey)
				if err != nil {
					return err
				}

				config := &tls.Config{
					Certificates: []tls.Certificate{cert},
				}
				conn, connErr = tls.Dial("tcp", f.config.ConnectAddr, config)
			} else {
				// Connect over raw TCP.
				conn, connErr = net.Dial("tcp", f.config.ConnectAddr)
			}
			if connErr != nil {
				return connErr
			}

			logger.Infow("connected federation client",
				"peer", conn.RemoteAddr())

			// Now that we have a network connection, instantiate a Channel.
			ch, err := cot.NewChannel(conn, conn)
			if err != nil {
				return err
			}

			// And send off the registration.
			f.register <- register{f.nextID(), ch}
			return nil
		}(); err != nil {
			f.logger.Named("connect").Sugar().Errorf("failed to connect federation client: %v", err)
		}
	}()

	f.main()
	return nil
}

// main is the main loop of this service.
func (f *Federation) main() {
	clients := make(map[int64]*Client)

	logger := f.logger.Named("main").Sugar()
	logger.Infof("starting")

L:
	for {
		select {
		case ev := <-f.handle.Events:
			// We've got an event from the bus. Send it to Clients.
			// TODO: If this is not a broadcast event, send it only to the responsible Client?
			for _, client := range clients {
				logger.Debugf("sending %+v", ev)
				if err := client.send(ev); err != nil {
					// TODO: Handle this error (kick the client?).
				}
			}
		case ev := <-f.recv:
			// We've got an event from a client. Send it to the bus if we haven't seen it already.
			for _, via := range ev.Via {
				if via == f.uid {
					continue L
				}
			}

			callsign, err := ev.Detail.DestinationCallsign()
			if err != nil {
				logger.Errorf("failed to extract destination callsign from received event: %v", err)
				continue
			}

			// Add ourselves to the via.
			ev.Via = append(ev.Via, f.uid)

			logger.Debugf("received %+v", ev)

			if callsign != "" {
				f.bus.PublishToCallsign(callsign, ev)
			} else {
				f.bus.Broadcast(ev)
			}
		case reg := <-f.register:
			// A new connection.
			c := &Client{
				id:      reg.id,
				channel: reg.channel,
			}

			clients[c.id] = c
			go c.run(f.logger.Named(fmt.Sprintf("client-%v", c.id)), f.deregister, f.recv)
		case id := <-f.deregister:
			// A disconnection.
			c, ok := clients[id]
			if ok {
				if err := c.Close(); err != nil {
					// TODO: Handle or log this error.
				}
				logger.Infof("deregistering client %v", id)
				delete(clients, id)
			}
		}
	}
}

// newFederation returns a new instance of Federation.
func newFederation() *Federation {
	return &Federation{
		recv:       make(chan model.Event),
		register:   make(chan register),
		deregister: make(chan int64),
	}
}

// Close closes the resources associated with this service.
func (f *Federation) Close() error {
	// TODO: Close the main loop.
	f.handle.Close()
	return nil
}

// Start starts this federation service.
func (f *Federation) Start(logger *zap.Logger, bus *cot.EventBus, decl services.Declaration) error {
	var config Config
	if err := decl.Decode(&config); err != nil {
		return err
	}

	if err := config.Validate(); err != nil {
		return err
	}

	f.uid = config.uidOr()
	f.logger = logger.Named(f.uid)
	f.config = config
	f.bus = bus
	f.handle = bus.SubscribeAll()

	if f.config.ListenAddr != "" {
		return f.listen()
	}

	return f.connect()
}

func init() {
	// Register ourselves.
	services.Register("federation", func() services.Service {
		return newFederation()
	})
}
