package federation

import (
	"io"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
	"bitbucket.org/calc0000/tak/cot/model"
)

// Client is a connected federation client. This can represent
// an outgoing connection, in the case of a "connect" federation,
// or it can be an incoming connection, as in the case of a "listen"
// federation.
type Client struct {
	// id is the ID of this client.
	id int64

	// channel is the transport to send/receive Events on.
	channel *cot.Channel
}

// Close closes the resources associated with this client.
func (c *Client) Close() error {
	return c.channel.Close()
}

// send an Event on this Client's Channel.
func (c *Client) send(ev model.Event) error {
	return c.channel.Send(ev)
}

// run is this Client's main loop.
func (c *Client) run(logger *zap.Logger, deregister chan<- int64, send chan<- model.Event) {
	defer func() {
		deregister <- c.id
	}()

L:
	for {
		select {
		case ev := <-c.channel.Receive:
			if ev.Err != nil {
				// EOF is a clean disconnect.
				if ev.Err != io.EOF {
					logger.Sugar().Errorf("received error: %v", ev.Err)
				}

				break L
			}

			// Just send this along to the federation service.
			send <- ev.Event
		}
	}
}
