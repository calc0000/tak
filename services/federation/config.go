package federation

import (
	"encoding/base64"
	"fmt"
	"math/rand"
)

// Config is a Federation service config.
type Config struct {
	// UID is an optionally provided UID string for the federation service.
	UID string `json:"uid,omitempty"`

	// ListenAddr is an address to listen on. This must not be specified
	// if a ConnectAddr is specified.
	ListenAddr string `json:"listen"`

	// ConnectAddr is an address to connect to. This must not be specified
	// if a ListenAddr is specified.
	ConnectAddr string `json:"connect"`

	// CertificateChain is an optionally specified file containing a PEM-encoded
	// X509 certificate chain. For "listen" Federations, this will be the certificate
	// chain presented to incoming connections. For "connect" Federations, this
	// will be the certificate chain presented to the server. In either case,
	// PrivateKey must also be specified.
	CertificateChain string `json:"cert-chain"`

	// PrivateKey is an optionally specified file containing a PEM-encoded
	// X509 private key. This must be specified if CertificateChain is specified.
	PrivateKey string `json:"private-key"`

	// TODO: Add client certificate verification.
}

// Validate validates the config, returning an error if it is invalid.
func (c *Config) Validate() error {
	if (c.ListenAddr != "" && c.ConnectAddr != "") || (c.ListenAddr == "" && c.ConnectAddr == "") {
		return fmt.Errorf("specify exactly one of \"listen\" and \"connect\"")
	}

	if c.CertificateChain != "" && c.PrivateKey == "" {
		return fmt.Errorf("\"private-key\" must be specified if \"cert-chain\" is specified")
	}

	return nil
}

// uidOr returns the configured UID, or a random string.
func (c *Config) uidOr() string {
	if c != nil && c.UID != "" {
		return c.UID
	}

	uidBuf := make([]byte, 16)
	rand.Read(uidBuf)
	return base64.StdEncoding.EncodeToString(uidBuf)
}
