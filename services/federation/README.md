# `federation`

The `federation` service provides a CoT relay between two `tak` instances, optionally encapsulated with TLS. This can be used to connect instances run by counterparties without completely exposing networks to each other. Effectively, the service sends a copy of all CoT events on its local bus to its peer, and publishes a copy of all CoT events received from its peer to its local bus.

The `federation` service can be run in one of two modes:
* `listen`: Listen for incoming connections.
* `connect`: Connect to an already listening federation service.

## Configuration

The `federation` service supports the following configuration options:

* `"uid"`: optional string, specifies a UID for the service to use. If left unspecified, a random string will be generated.
* `"listen"` or `"connect"`: required string, specifies the address to listen on, or connect to. Exactly one of these must be specified.
* `"cert-chain"`: optional string, specifies a path to a PEM-encoded X509 certificate chain. For `federation` services in `listen` mode, this certificate chain will be presented to connecting clients. In `connect` mode, this certificate chain will be presented as the client certificate. If not specified, the `federation` connection(s) will be in the clear.
* `"private-key"`: optional string, specifies a path to a PEM-encoded X509 private key. This must be specified if `"cert-chain"` is also specified.

## Caveats

The implementation of the `federation` service is likely incompatible with other TAK server implementations (including the official TAKserver). Specifically, to prevent infinite CoT event reflection, an additional child is added to the `<event>` XML element with the UIDs of the `federation` services that have already seen the event.
