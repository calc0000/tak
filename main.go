// package main implements a TAK server process.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"sync"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/cot"
	"bitbucket.org/calc0000/tak/services"

	_ "bitbucket.org/calc0000/tak/services/cotserver"
	_ "bitbucket.org/calc0000/tak/services/federation"
)

func main() {
	flag.Parse()

	// Read our config.
	var config Config
	var err error

	config.Logging = zap.NewDevelopmentConfig()

	if filename := flag.Arg(0); filename != "" {
		err = config.loadFrom(filename)
	} else {
		err = json.Unmarshal([]byte(defaultConfig), &config)
	}
	if err != nil {
		panic(fmt.Sprintf("failed to load config: %v", err))
	}

	// Always log to stderr.
	addStderr := true
	for _, path := range config.Logging.OutputPaths {
		if path == "stderr" {
			addStderr = false
			break
		}
	}

	if addStderr {
		config.Logging.OutputPaths = append(config.Logging.OutputPaths, "addStderr")
	}

	// Create our logger.
	logger, err := config.Logging.Build()
	if err != nil {
		fmt.Printf("failed to create logger: %v\n", err)
	}

	// Create our event bus.
	bus := cot.NewEventBus()

	// Use a WaitGroup to wait for all services to finish executing (this will probably never happen).
	wg := sync.WaitGroup{}
	for name, service := range config.Services {
		wg.Add(1)
		go func(name string, service services.Declaration) {
			defer wg.Done()

			if err := services.Start(logger.Named(name), bus, service); err != nil {
				logger.Sugar().Errorf("failed to start service \"%s\": %v", name, err)
			}
		}(name, service)
	}

	wg.Wait()
}

// defaultConfig is a config for a basic TAK server, usable out of the box.
const defaultConfig string = `
{
    "logging": {
        "level": "debug"
    },
    "services": {
        "server": {
            "service": "cotserver",
            "listen": ":8089",
            "semantics": "new"
        }
    }
}
`
