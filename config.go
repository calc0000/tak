package main

import (
	"encoding/json"
	"os"

	"go.uber.org/zap"

	"bitbucket.org/calc0000/tak/services"
)

// Config is a server config.
type Config struct {
	// Logging is the server's logging configuration.
	Logging zap.Config `json:"logging"`

	// Services is the mapping of service name to service declaration.
	Services map[string]services.Declaration `json:"services"`
}

func (c *Config) loadFrom(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(c); err != nil {
		return err
	}

	return nil
}
