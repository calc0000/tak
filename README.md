# tak

`tak` is an experimental server for [ATAK](https://en.wikipedia.org/wiki/Android_Team_Awareness_Kit) and ATAK-like client software.

## Quickstart

To quickly get started, grab the correct binary for your platform from [the downloads page](https://bitbucket.org/calc0000/tak/downloads/) and run it. The default configuration consists of a CoT server on port 8089 without SSL, and logging to `stderr`.

## Configuration

`tak` is configured by way of a JSON file, the path to which is provided on the command line as the sole argument to the binary (see `config.example.json` for an example). The config file should have two major sections: `logging` and `services`.

### logging section

The `logging` section of the config file contains options for configuring logging functionality. This section makes use of the underlying library's config facility, so refer to [that documentation](https://pkg.go.dev/go.uber.org/zap?tab=doc#Config) for a comprehensive reference. The most useful keys are probably:

* `level`: The minimum level of log messages to emit
* `outputPaths`: An array of targets to send log messages to

For example, to emit only info logs, and to send those logs to a file, specify something like:

```
{
    "logging": {
        "level": "info",
        "outputPaths": ["/var/log/tak/info.tak"]
    },
    // ...
}
```

### services section

The `services` section of the config file should be a JSON object, where the keys are names to give to service instsances, and the values are JSON objects describing the service to be run (see the Architecture section for more details). There are currently two services implemented:

* [`cotserver`](services/cotserver/README.md): Provides TCP server functionality to CoT clients.
* [`federation`](services/federation/README.md): Relays CoTs between CoT servers.

#### `federation` configuration

The `federation` service supports the following configuration options:

## Architecture

The [`EventBus`](cot/event-bus.go) is the abstraction at the core of the `tak` server. An `EventBus` serves as a basic message broker: clients can subscribe to the bus to receive CoT events, and can publish events to the bus. All `tak` services in a process share a single instance of an `EventBus`, so that future functionality is relatively easy to implement.
