package cot

import (
	"encoding/xml"
	"fmt"
	"io"
	"strings"

	"bitbucket.org/calc0000/tak/cot/model"
)

// Event is a CoT event, or an error.
type Event struct {
	model.Event
	Err error
}

// Channel is a bidirectional channel for reading/writing CoTs in XML format.
type Channel struct {
	// Receive is a channel on which to receive CoT events, or errors.
	Receive <-chan Event

	// send is a channel on which to send buffers to be sent to the client.
	send chan<- []byte

	// closers is a list of io.Closers to close when this Channel is closed.
	closers []io.Closer
}

// Close closes this channel. This signals the send goroutine that no further writes will be
// requested.
func (c *Channel) Close() error {
	close(c.send)

	var errs []string
	for _, closer := range c.closers {
		if err := closer.Close(); err != nil {
			errs = append(errs, err.Error())
		}
	}

	if len(errs) > 0 {
		return fmt.Errorf("failed to close channel: %v", strings.Join(errs, ", "))
	}
	return nil
}

// read is a goroutine that reads XML formatted events from the provided io.Reader,
// and sends them on the provided channel for consumption.
func (c *Channel) read(r io.Reader, ch chan<- Event) {
	defer close(ch)

	// CoT events are just sent as sequentially provided XML-encoded model.Events.
	decoder := xml.NewDecoder(r)

	for {
		var ev model.Event
		if err := decoder.Decode(&ev); err != nil {
			ch <- Event{
				Err: err,
			}
			return
		}

		ch <- Event{
			Event: ev,
		}
	}
}

// Send sends the provided model.Event on this channel, first marshaling it to XML.
func (c *Channel) Send(ev model.Event) error {
	buf, err := xml.Marshal(ev)
	if err != nil {
		return err
	}

	c.send <- buf
	return nil
}

// write is a goroutine that writes buffers to the provided io.Writer. The buffers arrive
// on the (synchronous) provided channel, and are stored in a buffer maintained by this
// goroutine. This way, Send operations on the Channel are as non blocking as possible.
func (c *Channel) write(w io.Writer, ch <-chan []byte) {
	var buffer [][]byte

	// The actual write loop.
	write := make(chan []byte)
	go func() {
		for buf := range write {
			if _, err := w.Write(buf); err != nil {
				// TODO: Handle.
				break
			}
		}
	}()

L:
	for {
		if len(buffer) == 0 {
			// Empty buffer.
			buffer = append(buffer, <-ch)
			continue
		}

		// The buffer has items here, so try to write, or otherwise wait for the buffer.
		select {
		case buf, ok := <-ch:
			if !ok {
				// We were closed.
				break L
			}

			buffer = append(buffer, buf)
		case write <- buffer[0]:
			buffer = buffer[1:]
		}
	}
}

// NewChannel creates a new channel that uses the provided io objects. Events can be sent
// to the channel via Send, and can be read by reading from the Receive channel.
func NewChannel(r io.ReadCloser, w io.WriteCloser) (*Channel, error) {
	write := make(chan []byte)
	read := make(chan Event)

	c := &Channel{
		Receive: read,
		send:    write,
		closers: []io.Closer{r, w},
	}

	go c.read(r, read)
	go c.write(w, write)

	return c, nil
}
