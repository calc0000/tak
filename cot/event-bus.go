package cot

import (
	"sync/atomic"

	"bitbucket.org/calc0000/tak/cot/model"
)

type registrationID struct {
	uid string
	id  int64
}

type register struct {
	registrationID
	ch chan<- model.Event
}

type registerBroadcastOnly struct {
	id int64
	ch chan<- model.Event
}

type registerAll struct {
	id int64
	ch chan<- model.Event
}

type targetedEvent struct {
	target string
	event  model.Event
}

type EventBus struct {
	// id is the next ID to use for subscribers. This field should only be used
	// via the nextID() method.
	id int64

	// broadcast is a channel for events to be broadcast to all subscribers.
	broadcast chan model.Event

	// publishToUID is a channel for events to be sent to a specific UID.
	publishToUID chan targetedEvent

	// publishToCallsign is a channel for events to be sent to a specific callsign.
	publishToCallsign chan targetedEvent

	// register is a channel for new subscribers.
	register chan register

	// registerBroadcastOnly is a channel for subscribers that only receive broadcast messages.
	registerBroadcastOnly chan registerBroadcastOnly

	// registerAll is a channel for subscribers that receive all events on the bus.
	registerAll chan registerAll

	// deregister is a channel for subscribers that are disconnecting.
	deregister chan registrationID

	// deregisterBroadcastOnly is a channel for broadcast only subscribers that are disconnecting.
	deregisterBroadcastOnly chan int64

	// deregisterAll is a channel for subscribers that receive all events that are disconnecting.
	deregisterAll chan int64

	// close_ is a channel to signal to the bus's loop that the bus has been closed.
	close_ chan bool
}

func (b *EventBus) nextID() int64 {
	return atomic.AddInt64(&b.id, 1)
}

func (b *EventBus) main() {
	// registrations is a map from UID to channels to send events on.
	registrations := make(map[string]map[int64]chan<- model.Event)
	broadcastOnly := make(map[int64]chan<- model.Event)
	all := make(map[int64]chan<- model.Event)

	callsignToUID := make(map[string]string)
	uidToCallsign := make(map[string]string)

main:
	for {
		select {
		case ev := <-b.broadcast:
			// Sniff this event to determine any callsign to UID mappings.
			// Ignore errors.
			callsign, err := ev.Detail.Callsign()
			if err == nil && callsign != "" {
				callsignToUID[callsign] = ev.What.UID
				uidToCallsign[ev.What.UID] = callsign
			}

			for _, chs := range registrations {
				for _, ch := range chs {
					ch <- ev
				}
			}

			for _, ch := range broadcastOnly {
				ch <- ev
			}

			for _, ch := range all {
				ch <- ev
			}
		case ev := <-b.publishToUID:
			chs, ok := registrations[ev.target]
			if ok {
				for _, ch := range chs {
					ch <- ev.event
				}
			}

			for _, ch := range all {
				ch <- ev.event
			}
		case ev := <-b.publishToCallsign:
			uid, ok := callsignToUID[ev.target]
			if !ok {
				continue
			}

			chs, ok := registrations[uid]
			if ok {
				for _, ch := range chs {
					ch <- ev.event
				}
			}

			for _, ch := range all {
				ch <- ev.event
			}
		case reg := <-b.register:
			m, ok := registrations[reg.uid]
			if !ok {
				m = make(map[int64]chan<- model.Event)
				registrations[reg.uid] = m
			}

			registrations[reg.uid][reg.id] = reg.ch
		case reg := <-b.registerBroadcastOnly:
			broadcastOnly[reg.id] = reg.ch
		case reg := <-b.registerAll:
			all[reg.id] = reg.ch
		case reg := <-b.deregister:
			// Delete this callsign.
			callsign, ok := uidToCallsign[reg.uid]
			if ok {
				delete(callsignToUID, callsign)
				delete(uidToCallsign, reg.uid)
			}

			m, ok := registrations[reg.uid]
			if ok {
				ch, ok := m[reg.id]
				if ok {
					close(ch)
					delete(m, reg.id)
				}

				if len(m) == 0 {
					delete(registrations, reg.uid)
				}
			}
		case id := <-b.deregisterBroadcastOnly:
			ch, ok := broadcastOnly[id]
			if ok {
				close(ch)
				delete(broadcastOnly, id)
			}
		case id := <-b.deregisterAll:
			ch, ok := all[id]
			if ok {
				close(ch)
				delete(all, id)
			}
		case ev := <-b.close_:
			if ev {
				break main
			}
		}
	}

	for _, chs := range registrations {
		for _, ch := range chs {
			close(ch)
		}
	}
}

func NewEventBus() *EventBus {
	b := &EventBus{
		broadcast:               make(chan model.Event),
		publishToUID:            make(chan targetedEvent),
		publishToCallsign:       make(chan targetedEvent),
		register:                make(chan register),
		registerBroadcastOnly:   make(chan registerBroadcastOnly),
		registerAll:             make(chan registerAll),
		deregister:              make(chan registrationID),
		deregisterBroadcastOnly: make(chan int64),
		deregisterAll:           make(chan int64),
		close_:                  make(chan bool),
	}

	go b.main()
	return b
}

func (b *EventBus) Close() {
	b.close_ <- true
}

func (b *EventBus) SubscribeBroadcastOnly() BroadcastOnlyHandle {
	busCh := make(chan model.Event, 1)
	subscriberCh := make(chan model.Event, 1)
	id := b.nextID()

	go forwarder(subscriberCh, busCh)

	b.registerBroadcastOnly <- registerBroadcastOnly{id, busCh}
	return BroadcastOnlyHandle{
		Events:     subscriberCh,
		id:         id,
		deregister: b.deregisterBroadcastOnly,
	}
}

func (b *EventBus) SubscribeAll() AllHandle {
	busCh := make(chan model.Event, 1)
	subscriberCh := make(chan model.Event, 1)
	id := b.nextID()

	go forwarder(subscriberCh, busCh)

	b.registerAll <- registerAll{id, busCh}
	return AllHandle{
		Events:     subscriberCh,
		id:         id,
		deregister: b.deregisterAll,
	}
}

func (b *EventBus) Subscribe(uid string) Handle {
	// Add a buffer in case a client has already stopped reading from the Handle, but there
	// is still a message on the way.
	busCh := make(chan model.Event, 1)
	subscriberCh := make(chan model.Event, 1)
	id := registrationID{uid, b.nextID()}

	go forwarder(subscriberCh, busCh)

	b.register <- register{id, busCh}

	return Handle{
		Events:     subscriberCh,
		id:         id,
		deregister: b.deregister,
	}
}

func (b *EventBus) PublishToUID(target string, event model.Event) {
	b.publishToUID <- targetedEvent{target, event}
}

func (b *EventBus) PublishToCallsign(target string, event model.Event) {
	b.publishToCallsign <- targetedEvent{target, event}
}

func (b *EventBus) Broadcast(event model.Event) {
	b.broadcast <- event
}

type AllHandle struct {
	Events     <-chan model.Event
	id         int64
	deregister chan<- int64
}

func (h *AllHandle) Close() {
	h.deregister <- h.id
}

type BroadcastOnlyHandle struct {
	Events     <-chan model.Event
	id         int64
	deregister chan<- int64
}

func (h *BroadcastOnlyHandle) Close() {
	h.deregister <- h.id
}

type Handle struct {
	Events     <-chan model.Event
	id         registrationID
	deregister chan<- registrationID
}

func (h *Handle) Close() {
	h.deregister <- h.id
}

func forwarder(out chan<- model.Event, in <-chan model.Event) {
	var buffer []model.Event

L:
	for {
		if len(buffer) == 0 {
			select {
			case ev, ok := <-in:
				if !ok {
					break L
				}

				buffer = append(buffer, ev)
			}
		} else {
			select {
			case ev, ok := <-in:
				if !ok {
					break L
				}

				buffer = append(buffer, ev)
			case out <- buffer[0]:
				buffer = buffer[1:]
			}
		}
	}

	close(out)
}
