package model

import (
	"encoding/xml"
	"strings"
	"time"
)

// TypeHint is a list of type identifiers.
// TypeHint values loosely represent a path through an object type tree.
type TypeHint []string

// String returns this TypeHint as a string: its components joined with "-".
func (h *TypeHint) String() string {
	return strings.Join([]string(*h), "-")
}

// ParseTypeHint parses a TypeHint from a string.
func ParseTypeHint(s string) TypeHint {
	var h TypeHint
	h.UnmarshalText([]byte(s))
	return h
}

// UnmarshalText unmarshals a TypeHint from a string representation.
func (h *TypeHint) UnmarshalText(text []byte) error {
	*h = TypeHint(strings.Split(string(text), "-"))
	return nil
}

// MarshalText marshals a TypeHint to a string representation.
func (h TypeHint) MarshalText() ([]byte, error) {
	return []byte(strings.Join([]string(h), "-")), nil
}

// Timestamp is a timestamp.
type Timestamp time.Time

// timestampLayout is the layout used for formatting Timestamp values in COT XML representations.
// Ostensibly, the timestamp values are in ISO 8601, but in lieu of using a full parser, this
// simple layout is used.
const timestampLayout string = "2006-01-02T15:04:05Z"

// String returns this timestamp in ISO 8601 form.
func (t *Timestamp) String() string {
	return time.Time(*t).Format(timestampLayout)
}

// UnmarshalText parses a timestamp from an ISO 8601 string form.
func (t *Timestamp) UnmarshalText(text []byte) error {
	time, err := time.Parse(timestampLayout, string(text))
	*t = Timestamp(time)
	return err
}

// MarshalText marshals this timestamp to an ISO 8601 string form.
func (t Timestamp) MarshalText() ([]byte, error) {
	return []byte(t.String()), nil
}

// Any represents an arbitrary object unmarshaled from XML.
type Any struct {
	// XMLName is the XML name of this object.
	XMLName xml.Name
	// Children is the list of XML objects that are children of this object.
	Children []*Any `xml:",any"`
	// Attrs is the list of XML attributes of this object.
	Attrs []xml.Attr `xml:",any,attr"`
	// CharData is the character data of this object.
	CharData string `xml:",chardata"`
}

// String returns the XML representation of this object.
func (a *Any) String() string {
	return ""
	//return string(a.InnerXML)
}

// What contains the "what" information about an event.
type What struct {
	// UID is the uid of the object associated with this event.
	UID string `xml:"uid,attr"`

	// Type is the type of this event.
	Type TypeHint `xml:"type,attr"`
}

// When contains the "when" information about an event.
type When struct {
	// Time is when this event was created.
	Time Timestamp `xml:"time,attr"`

	// Start is the first time when this event is actionable (this may be arbitrarily different from Time).
	Start Timestamp `xml:"start,attr"`

	// Stale is the last time when this event is actionable.
	Stale Timestamp `xml:"stale,attr"`
}

// Where contains the "where" information about an event.
type Where struct {
	// How contains information about how the location information in Point was computed.
	How TypeHint `xml:"how,attr"`

	// Point is the location associated with this event.
	Point Point `xml:"point"`
}

// Event is a COT event.
type Event struct {
	// XMLName is the XML name of this event.
	XMLName xml.Name `xml:"event"`

	// What is the "what" meta information about this event.
	What

	// When is the "when" meta information about this event.
	When

	// Where is the "where" meta information about this event.
	Where

	// Via is a list of federation services that this event has passed through.
	// TODO: Is this the way TAKserver does it? Does that matter?
	Via []string `xml:"via"`

	// Detail is the optional, arbitrary, additional information associated with this event.
	Detail Detail `xml:"detail"`
}

// Point is a geographical location.
type Point struct {
	Latitude      float64 `xml:"lat,attr"`
	Longitude     float64 `xml:"lon,attr"`
	Height        float64 `xml:"hae,attr"`
	LinearError   float64 `xml:"le,attr"`
	CircularError float64 `xml:"ce,attr"`
}
