package model

import (
	"encoding/xml"
)

// Link is a sometimes present detail element indicating a target for this event.
type Link struct {
	// UID is the target UID for this event.
	UID string `xml:"uid,attr"`

	// Timestamp is the timestamp of this event.
	Timestamp Timestamp `xml:"production_timestamp,attr"`

	// Callsign is the callsign of the contact with the specified UID.
	Callsign string `xml:"parent_callsign,attr"`

	// Relation is the relation between the sender of this event and the receiver.
	Relation string `xml:"relation,attr"`
}

// Group is a detail element that denotes this UID as a team member.
type Group struct {
	// Group is the group this UID is a member of.
	Group string `xml:"name,attr"`

	// Role is the role this UID has in the group.
	Role string `xml:"role,attr"`
}

// Detail is a wrapper around Any to provide an Unmarshal interface.
type Detail Any

// Unmarshal unmarshals this Detail's XML representation into an object.
func (d *Detail) Unmarshal(v interface{}) error {
	// Surround the actual XML with a <detail> tag to ease unmarshaling.
	b, err := xml.Marshal(d)
	if err != nil {
		return err
	}

	return xml.Unmarshal(b, v)
}

// Link returns a link encapsulated in this Detail, if there is one.
func (d *Detail) Link() (*Link, error) {
	var temp struct {
		Link *Link `xml:"link"`
	}

	err := d.Unmarshal(&temp)
	return temp.Link, err
}

// Callsign returns a callsign encapsulated in this Detail.
func (d *Detail) Callsign() (string, error) {
	var temp struct {
		Contact struct {
			Callsign string `xml:"callsign,attr"`
		} `xml:"contact"`
	}

	err := d.Unmarshal(&temp)
	return temp.Contact.Callsign, err
}

// DestinationCallsign resturns a destination callsign encapsulated in this Detail.
func (d *Detail) DestinationCallsign() (string, error) {
	var temp struct {
		Marti struct {
			Destination struct {
				Callsign string `xml:"callsign,attr"`
			} `xml:"dest"`
		} `xml:"marti"`
	}

	err := d.Unmarshal(&temp)
	return temp.Marti.Destination.Callsign, err
}

// Group returns a Group encapsulated in this detail, if there is one.
func (d *Detail) Group() (*Group, error) {
	var temp struct {
		Group *Group `xml:"__group"`
	}

	err := d.Unmarshal(&temp)
	return temp.Group, err
}
